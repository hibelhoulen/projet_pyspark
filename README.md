
# Churn prédiction

Dans ce projet, PySpark est utilisé pour prédire le roulement basé sur un ensemble de données de 120MB d’une plate-forme fictionnelle de service de musique fictive, "Sparkify". 

## Getting Started

Les instructions ci-dessous vous aideront à configurer votre machine locale pour exécuter la copie de ce projet.
Vous pouvez exécuter l’ordinateur portable en mode local sur un ordinateur local ainsi que sur un cluster d’un fournisseur de cloud comme AWS ou IBM Cloud.

### Prérequis

#### Software et Data 

  - Anaconda 3
  - Python 3.7.+
  - pyspark 2.4


## 1. Motivation

Sparkify est un service de musique numérique similaire à Spotify et fizy. De nombreux utilisateurs font des abonnements, écoutent leurs chansons préférées avec des abonnements gratuits (avec publicité) ou payants, ajoutent des amis ou des chansons à leurs listes de lecture, etc. Les utilisateurs peuvent également mettre à niveau, réduire ou annuler vos abonnements à tout moment. À l’instar de tous les services numériques et de leurs abonnements, le roulement des membres est le problème le plus important, de sorte que l’entreprise devrait définir un modèle pour identifier les clients qui sont potentiellement désavantagés et faire des offres de marketing pour conserver leurs abonnements.

## 2. Datasets

User activity dataset from Udacity
The dataset logs user demographic information (e.g. user name, gender, location State) and activity (e.g. song listened, event type, device used) at individual timestamps.
A small subset (~120MB) of the full dataset was used for the entire analysis due to budjet limitations to run the models in a cluster
https://www.kaggle.com/code/nilaychauhan/pyspark-tutorial-for-beginners/data

## Problème :

est que la prévision des clients potentiels qui vont churn de Sparkify service de musique numérique.

## 3. Data Exploration

- Chargement des données
- Évaluer les valeurs manquantes
- Analyse exploratoire des données
- Vue d’ensemble des colonnes numériques : statistiques descriptives
- Comparer le comportement des utilisateurs non utilisateurs par rapport aux autres en ce qui concerne :
1. Session length
2. Numbers of sessions
3. Gender
4. Event types (e.g. add a friend, advertisement, thumbs up)
- Feature engineering pour l’apprentissage automatique
- séparés training et testing data
- Choisir des mesures d’évaluation
- créer un pipeline de validation croisée, former des modèles d’apprentissage automatique et évaluer les performances des modèles
- évaluation initiale du modèle avec :
1. Logistic regression
2. Random forest 
3. Gradient-boosted tree 
- Évaluer le rendement du modèle
- Évaluer l’importance des caractéristiques

## 4. Resultats

Performance du model sur testing data:


![Other Metrics](data/res.jpg)