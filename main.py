from utility.session_spark import *
from featureEngineer.exploredata import *
from algo.algor import *
from visual.vis import *
from featureEngineer.cleandata import *
from featureEngineer.selection import *


if __name__ == '__main__':
    print("create a Spark session")
    spark = create_spark_session()
    print(spark.sparkContext.appName)
    print("****Load DATA **********")
    df = load_dataset(spark)
    print(" ----------------------------------- Clean Dataset **------------")
    log_events = clean_func(df)
    print("***********************  explore Dataset    *************")
    log_events = create_churn_col(log_events)
    log_events = create_phase_col(log_events)
    print("***********features selection ****************")
    features_selection(spark)
    print("**********************   modeling section     **************")
    apply_algorithm(spark)
    visual_result_algo()
    feature_importance_rf()
    lr_df = load_dataset_csv(spark, "data/lr_df.csv")
    gbt_df = load_dataset_csv(spark, "data/gbt_df.csv")
    grid_df = load_dataset_csv(spark, "data/grid_df.csv")
    create_nan(grid_df, "Random Forest")
    create_nan(lr_df, "Logistic Regression")
    create_nan(gbt_df, "Gradient Boosted Trees")

    spark.stop()
    print("fin******************")
