# import libraries
from pyspark.ml import Pipeline
from pyspark.ml.classification import LogisticRegression, RandomForestClassifier, GBTClassifier
from pyspark.ml.evaluation import MulticlassClassificationEvaluator
from pyspark.ml.feature import StandardScaler, VectorAssembler, StringIndexer, OneHotEncoder
from pyspark.ml.tuning import CrossValidator, ParamGridBuilder
from metrix.metrixFunc import *


def apply_algorithm(spark):
    train = spark.read.load("data/train.parquet")
    test = spark.read.load("data/test.parquet")
    data = spark.read.load("data/data.parquet")
    cols = data.drop('label').drop('userId').columns
    rf_test_evaluator, rf_yhat_test, rf = results_classifier(train, test, cols, param_grid=1, rf=True)
    print("****** Fit RandomForestClassifier successfully *********** ")
    lr_test_evaluator, lr_yhat_test, lr = results_classifier(train, test, cols, param_grid=1, lr=True)
    print("****** Fit LogisticRegression successfully  ***********")
    gbt_test_evaluator, gbt_yhat_test, gbt = results_classifier(train, test, cols, param_grid=1)
    print("****** Fit GBTClassifier successfully ***********")
    cross_val, grid_evaluator, grid_yhat_test, grid_best_model = results_rf_classifier(train, test, cols, param_grid=2)

    rf_metrics, rf_matrix = save_metrics(rf_test_evaluator, rf_yhat_test)
    print("*****metrix of RandomForestClassifier*********")
    print(rf_metrics)
    print("**********metrix of LogisticRegression*******************")
    lr_metrics, lr_matrix = save_metrics(lr_test_evaluator, lr_yhat_test)
    print(lr_metrics)
    print("***********metrix of GBTClassifier**********")
    gbt_metrics, gbt_matrix = save_metrics(gbt_test_evaluator, gbt_yhat_test)
    print(gbt_metrics)
    rf_grid_metrics, rf_grid_matrix = save_rf_metrics(grid_evaluator, grid_yhat_test)
    print("****************rf_grid_metrics************************")
    print(rf_grid_metrics)


def results_classifier(train, test, cols, param_grid, lr=False, rf=False):

    model = train_model(train, cols, param_grid, lr, rf)  # train model on train set
    yhat_test = model.transform(test)
    test_evaluator = MulticlassClassificationEvaluator(predictionCol="prediction", labelCol="label")
    best_model = model.bestModel  # save the best model of the pipeline

    return test_evaluator, yhat_test, best_model


def train_model(train, cols, param_grid, lr=False, rf=False):
    if lr:
        classifier = LogisticRegression(labelCol="label", featuresCol="scaled")
    elif rf:
        classifier = RandomForestClassifier(labelCol="label", featuresCol="scaled")
    else:
        classifier = GBTClassifier(labelCol="label", featuresCol="scaled")

    if param_grid == 1:
       param_grid = ParamGridBuilder().build()
    elif param_grid == 2:
         param_grid = ParamGridBuilder() \
                      .addGrid(classifier.numTrees, [5, 10, 20, 25, 30, 50, 100]) \
                      .build()  # build parameters

    assembler = VectorAssembler(inputCols=cols, outputCol="features")
    sd_scaler = StandardScaler(inputCol="features", outputCol="scaled", withStd=True, withMean=True)
    pipeline = Pipeline(stages=[assembler, sd_scaler, classifier])
    # Cross validation
    cross_val = CrossValidator(
        estimator=pipeline,
        estimatorParamMaps=param_grid,
        evaluator=MulticlassClassificationEvaluator(metricName='f1'),
        numFolds=3
    )
    model = cross_val.fit(train)
    return model


def results_classifier(train, test, cols, param_grid, lr=False, rf=False):

    model = train_model(train, cols, param_grid, lr, rf)  # train model on train set
    yhat_test = model.transform(test)
    test_evaluator = MulticlassClassificationEvaluator(predictionCol="prediction", labelCol="label")
    best_model = model.bestModel  # save the best model of the pipeline

    return test_evaluator, yhat_test, best_model


def results_rf_classifier(train, test, cols, param_grid):
    cross_val = train_model(train, cols, param_grid, rf=True)
    grid_yhat_test = cross_val.transform(test)
    grid_evaluator = MulticlassClassificationEvaluator(predictionCol="prediction", labelCol="label")
    best_model = cross_val.bestModel  # save the best model of the pipeline

    return cross_val, grid_evaluator, grid_yhat_test, best_model