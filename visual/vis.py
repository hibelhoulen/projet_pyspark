# import libraries
from pyspark.mllib.linalg import SparseVector
import pyspark.sql.functions as F
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np


def visual_result_algo():
    result_dict = {'Random-Forest': [0.94, 0.91, 1700.99],
                   'Logistic Regression': [0.8363121797635641, 0.8227760264874404, 1322.0],
                   'GBTC': [0.8932296850198, 0.78328, 1376.17]}
    # create bar chart with scores and training times
    fig, ax1 = plt.subplots(figsize=(10, 10))
    # set width of bar
    barWidth = 0.25
    # set height of bar
    bars1 = [r[0] for r in result_dict.values()]
    bars2 = [r[1] for r in result_dict.values()]
    bars3 = [r[2] for r in result_dict.values()]
    # set position of bar on X axis
    r1 = np.arange(len(bars1))
    r2 = [x + barWidth for x in r1]
    r3 = [x + barWidth for x in r2]
    plot1 = ax1.bar(r1, bars1, color='lightgrey', width=barWidth, edgecolor='white', label='f1-train')
    plot2 = ax1.bar(r2, bars2, color='rosybrown', width=barWidth, edgecolor='white', label='f1-test')
    ax1.set_ylim(top=1)
    ax1.set_ylabel("F1-Score [-]")
    ax2 = ax1.twinx()
    plot3 = ax2.bar(r3, bars3, color='mistyrose', width=barWidth, edgecolor='white', label='time')
    ax2.set_ylim(bottom=0)
    ax2.set_ylabel("Training Time [s]")
    plt.legend([plot1, plot2, plot3], ["F1-train", "F1-test", "training time"])
    plt.xticks([r + barWidth for r in range(len(bars1))], result_dict.keys())
    plt.show()


def create_coef_df(cols, lr, rf, gbt, grid):
    lr_feature_coef = lr.stages[2].coefficients.values.tolist()
    lr_dict = dict(list(zip(cols, lr_feature_coef)))
    lr_df = pd.DataFrame.from_dict(lr_dict, orient='index', columns=['coefficient']) \
        .sort_values('coefficient', ascending=False)

    rf_feature_coef = rf.stages[2].featureImportances.values.tolist()
    rf_dict = dict(list(zip(cols, rf_feature_coef)))
    rf_df = pd.DataFrame.from_dict(rf_dict, orient='index', columns=['coefficient']) \
        .sort_values('coefficient', ascending=False)

    gbt_feature_coef = gbt.stages[2].featureImportances.values.tolist()
    gbt_dict = dict(list(zip(cols, gbt_feature_coef)))
    gbt_df = pd.DataFrame.from_dict(gbt_dict, orient='index', columns=['coefficient']) \
        .sort_values('coefficient', ascending=False)

    grid_feature_coef = grid.stages[2].featureImportances.values.tolist()
    grid_dict = dict(list(zip(cols, rf_feature_coef)))
    grid_df = pd.DataFrame.from_dict(rf_dict, orient='index', columns=['coefficient']) \
        .sort_values('coefficient', ascending=False)

    return lr_df, rf_df, gbt_df, grid_df


def create_nan(df, title):
    nan_dict = {}
    df = df.toPandas()
    for column in df.columns:
        nan_dict[column] = df[column].values.tolist()

    index = list(nan_dict['Index'])
    b = list(nan_dict['coefficient'])
    coef = list(map(float, b))
    plt.figure(figsize=(15, 15))
    sns.barplot(index, coef)
    plt.title(title, fontsize=40)
    plt.xlabel('Features', fontsize=30)
    plt.ylabel('Feature Importance', fontsize=30)
    plt.xticks(rotation=60, fontsize=20)
    plt.yticks(fontsize=14)
    plt.show()


def plot_coef(df, title):
    nan_dict = {}
    for col in df.columns:
        nan_dict[col] = df.where(df[col].isNull() | F.isnan(df[col])).select(col).count()
    plt.figure(figsize=(30, 15))
    sns.barplot(x=df.Index, y=df.coefficient, data=nan_dict)
    plt.title(title, fontsize=40)
    plt.xlabel('Features', fontsize=30)
    plt.ylabel('Feature Importance', fontsize=30)
    plt.xticks(rotation=60, fontsize=20)
    plt.yticks(fontsize=14)
    plt.show()


def feature_importance_rf():
    # feature importance of random forest
    categorical_columns = ["gender", "valid_level"]
    final_num_features = ['last_ts', 'duration', 'music_days', 'Thumbs Down_per_log', 'Submit Upgrade_per_day',
                          'NextSong_per_log', 'Add Friend_per_day', 'About', 'Submit Downgrade', 'Roll Advert',
                          'Home_per_day', 'Help_per_day', 'Downgrade', 'Add to Playlist', 'log_counts', 'avg_length',
                          'Upgrade_per_log', 'Thumbs Up_per_day', 'Settings_per_day', 'Save Settings_per_day',
                          'Logout_per_log', 'Error']
    # feature importance of random forest
    f_importance = [0.2188, 0.103, 0.0401, 0.0262, 0.0645, 0.0331, 0.0551, 0.0145, 0.0058, 0.0638, 0.0366, 0.0353,
                    0.0164, 0.0362, 0.0328, 0.0313, 0.0349, 0.0473, 0.042, 0.0109, 0.0292, 0.0066, 0.0088, 0.0067]
    f_names = final_num_features + categorical_columns
    data = {"feature_names": f_names,
            "feature_importance": f_importance,
            }
    features_rf = pd.DataFrame(data, columns=["feature_names", "feature_importance"])
    print(features_rf.sort_values(["feature_importance"], ascending=False))





