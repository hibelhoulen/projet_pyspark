# import libraries
import datetime
import pyspark.sql.functions as F
from pyspark.sql.functions import col, udf
from pyspark.sql.types import IntegerType, TimestampType
import pandas as pd
import numpy as np


def features_selection(spark):
    events = spark.read.json("mini_sparkify_event_data.json")
    # events where a userId is an empty string are not valid, remove these
    valid_events = events.where(col("userId") != "")
    print("Number of total events: {}; number of valid events {}".format(events.count(), valid_events.count()))
    print("Number of users: {}".format(valid_events.select("userId").distinct().count()))
    # udf for transforming timestamp to datetime
    get_date = udf(lambda x: datetime.datetime.fromtimestamp(x / 1000.0),
                   TimestampType())  # udf to convert to timestamp/date
    # create column with the date when the log entry was done
    valid_events = valid_events.withColumn("log_date", get_date(col("ts")))
    # udf that defines churn
    find_churn = udf(lambda x: 1 if x == "Cancellation Confirmation" else 0, IntegerType())
    # add a column "churn" to the dataframe indicating that a cancellation was confirmed
    valid_events = valid_events.withColumn("churn", find_churn(col("page")))
    # dataframe for the lengths users listen to music
    user_length = valid_events.groupBy("userId").agg({"length": "mean"}) \
        .withColumnRenamed("avg(length)", "avg_length")
    # some values can be set into relation to a certain time period, e.g. the duration the user is active
    # hence we need to create a df with all users and their active time period
    # first find the first and last log entry for each user and how much log entries exist per user (all actions)
    time_df = valid_events.groupBy(["userId"]) \
        .agg(F.sum("churn").alias("churned"), F.min("log_date").alias("first_log"),
             F.max("log_date").alias("last_log"), F.count("page").alias("log_counts"), F.max("ts").alias("last_ts"))

    # register the function as a udf
    get_time_difference_udf = udf(get_time_difference, IntegerType())

    # add the duration as a time difference between first and last log date
    time_df = time_df.withColumn("duration", get_time_difference_udf(col("first_log"), col("last_log"))) \
        .drop("first_log", "last_log").withColumnRenamed("churned", "label")
    # create a dummy dataframe where each action (About, Thumbs Up, ...) from page is a new column with the number
    # how often this action appeared in the data for each user
    dummy_df = valid_events.select("userId", "page").groupBy("userId").pivot("page") \
        .count().drop("Cancel", "Cancellation Confirmation")
    # fill null values
    dummy_df = dummy_df.na.fill(0)
    # last valid user level
    user_level = valid_events.orderBy("log_date", ascending=False).groupBy("userId").agg(
        F.first("level").alias('valid_level'))
    # gender of the users
    user_gender = valid_events.select(["userId", "gender"]).distinct()
    # calculate the total amount of days the user listened to music
    songs_per_date = valid_events.withColumn("date", F.to_date(col("log_date"))).where(col("page") == "NextSong") \
        .groupBy(["userId", "date"]).agg(F.lit(1).alias("played_music"))
    songs_per_day = songs_per_date.groupBy("userId").agg(F.sum("played_music").alias("music_days"))
    # join all previous created dataframes
    df = time_df.join(dummy_df, on="userId").join(user_level, on="userId") \
        .join(user_gender, on="userId").join(user_length, on="userId").join(songs_per_day, on="userId")

    # variables which shall be divided by a certain value like the duration or log count
    cols_to_divide = ['music_days', 'About', 'Add Friend', 'Add to Playlist', 'Downgrade', 'Error', 'Help', 'Home',
                      'Logout', 'NextSong', 'Roll Advert', 'Save Settings', 'Settings', 'Submit Downgrade',
                      'Submit Upgrade', 'Thumbs Down', 'Thumbs Up', 'Upgrade']
    # dataframe with new columns for the values per duration
    df_duration = divide_columns_by(df, cols_to_divide, "duration", "per_day")
    # final feature dataframe also containing the values per log count
    df_features = divide_columns_by(df_duration, cols_to_divide, "log_counts", "per_log")
    # calculate the number of sessions per user and the average number of sessions per day
    user_sessions = valid_events.groupBy(["userId"]).agg(F.countDistinct("sessionId").alias("number_sessions"))
    df_features = df_features.join(user_sessions, on="userId")
    df_features = df_features.withColumn("sessions_per_day", col("number_sessions") / col("duration"))
    # ratio of days the user actually listened to music
    df_features = df_features.withColumn("ratio_music_days", col("music_days") / col("duration"))
    # ratio of songs the user pressed the "Thumbs Up" button
    df_features = df_features.withColumn("like_ratio", col("Thumbs Up") / col("NextSong"))
    print("--------------------------------------------------------------------")
    # convert the dataframe to pandas for feature analysis
    pd_df = pd.read_csv('data/pd_df.csv')
    # maximum numbers of features to consider
    num_features = 25
    # split into numerical features and response variable
    X = pd_df.drop(["label"], axis=1).select_dtypes(include=["int64", "int32", "float64", "datetime64[ns]"])
    y = pd_df["label"]
    # select features on correlation
    cor_support, cor_features = cor_selector(X, y, num_features)
    # select features on correlation
    cor_support, cor_features = cor_selector(X, y, num_features)
    print(" Pearson feature selection success")
    # prepare feature importance dataframe
    feature_name = X.columns
    feature_selection_df = pd.DataFrame({'Feature': feature_name, 'Pearson': cor_support})
    # count the selected times for each feature
    feature_selection_df['Total'] = np.sum(feature_selection_df, axis=1)
    # sort values according to importance
    feature_selection_df = feature_selection_df.sort_values(['Total', 'Feature'], ascending=False)
    print(" pearson result")
    print(feature_selection_df)
    # store feature importance dataframe as csv-file
    feature_selection_df.to_csv("data/feature_selection_df.csv")


def get_time_difference(date_1, date_2):
    delta = date_2 - date_1
    if delta.days == 0:
        return 1
    else:
        return delta.days


def cor_selector(X, y, num_feats):
    cor_list = []
    feature_name = X.columns.tolist()
    # calculate the correlation with y for each feature
    for i in X.columns.tolist():
        cor = np.corrcoef(X[i], y)[0, 1]
        cor_list.append(cor)
    # replace NaN with 0
    cor_list = [0 if np.isnan(i) else i for i in cor_list]
    # feature name
    cor_feature = X.iloc[:, np.argsort(np.abs(cor_list))[-num_feats:]].columns.tolist()
    # feature selection? 0 for not select, 1 for select
    cor_support = [True if i in cor_feature else False for i in feature_name]
    return cor_support, cor_feature


def cor_selector(X, y, num_feats):
    cor_list = []
    feature_name = X.columns.tolist()
    # calculate the correlation with y for each feature
    for i in X.columns.tolist():
        cor = np.corrcoef(X[i], y)[0, 1]
        cor_list.append(cor)
    # replace NaN with 0
    cor_list = [0 if np.isnan(i) else i for i in cor_list]
    # feature name
    cor_feature = X.iloc[:, np.argsort(np.abs(cor_list))[-num_feats:]].columns.tolist()
    # feature selection? 0 for not select, 1 for select
    cor_support = [True if i in cor_feature else False for i in feature_name]
    return cor_support, cor_feature


# divide the actions by the amount of logs or the overall duration of their registration
def divide_columns_by(df, columns, value, appendix):
    for name in columns:
        new_name = name + "_" + appendix
        df = df.withColumn(new_name, col(name) / col(value))
    return df

