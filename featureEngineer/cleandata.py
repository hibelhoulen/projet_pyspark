import matplotlib.pyplot as plt
import seaborn as sns
import pyspark.sql.functions as F


def create_nan_dict(df):
    # create a dictionary keeping column names and nans
    nan_dict = {}
    for col in df.columns:
        nan_dict[col] = df.where(df[col].isNull() | F.isnan(df[col])).select(col).count()

        sorted_dict = dict(sorted(nan_dict.items(), key=lambda item: item[1]))
        # plot Null values
    sns.barplot([*sorted_dict.keys()], [*sorted_dict.values()])
    plt.title("NaN per Column")
    plt.xlabel("Dataset Column")
    plt.ylabel("NaN Count")
    plt.xticks(rotation=90)
    plt.yticks()
    plt.show()

    return sorted_dict


def clean_dataset(df):
    log_events = df.dropna(how="any", subset=["firstName"])
    log_events = log_events.withColumn('song', F.when(log_events.song.isNull(), F.lit("no_song")).otherwise(df.song))
    log_events = log_events.withColumn('artist',
                                       F.when(log_events.artist.isNull(), F.lit("no_artist")).otherwise(df.artist))
    log_events = log_events.withColumn('length', F.when(log_events.length.isNull(), F.lit(0)).otherwise(df.length))

    return log_events


def clean_func(df):
    df.head(5)
    df.printSchema()
    df.describe().show()
    df.count()
    create_nan_dict(df)
    log_events = clean_dataset(df)
    print("sessionID & userID vide ou null supprimé")
    create_bins_n_sums(log_events)
    log_events = fix_log_events(log_events)
    print(f'Fixed data Types')
    log_events = rename_columns(log_events)
    print(f'Renamed value Columns of page & location')
    # create_num_df(log_events)
    print(f'Created Numeric Features')
    log_events = get_device_col(log_events)
    print(f'Created device Column')
    bin_cols = ['gender', 'location', 'device']
    sum_cols = ['auth', 'level', 'method', 'page', 'status']
    print(f'Separating categorical columns into:\
    \nBinary Features to keep as is: {bin_cols} \
    \nFeatures to aggregate: {sum_cols}')
    # transform_the_cats(log_events)
    print(f'Fixed Categorical Features')

    return log_events


def get_device_col(log_events):
    get_device = F.udf(lambda x: x.split('(')[1].replace(";", " ").split(" ")[0])

    log_events = log_events.withColumn("device", get_device(log_events["userAgent"]))
    return log_events


def create_bins_n_sums(df):
    num_cols = []
    cat_cols = []

    for s in df.schema:
        data_type = str(s.dataType)
        if data_type == "StringType":
            cat_cols.append(s.name)

        if data_type == "LongType" or data_type == "DoubleType":
            num_cols.append(s.name)
    num_cols = n_cols = ['itemInSession', 'length', 'registration', 'sessionId', 'status', 'ts']
    cat_cols = c_cols = ['artist', 'auth', 'firstName', 'gender', 'lastName',
                         'level', 'location', 'method', 'page', 'song', 'userAgent', 'userId']
    print("numeric column : ", n_cols)
    print("categ column :", c_cols)

    return num_cols, cat_cols


# retype status bigint to string
def fix_log_events(log_events):
    log_events = log_events.withColumn('status', F.col('status').cast('string'))

    return log_events


# rename values of location & page
def rename_columns(log_events):
    get_location = F.udf(lambda location: location.split(", ")[-1])
    log_events = log_events.withColumn("location", get_location(log_events['location']))

    get_page = F.udf(lambda page: page.replace(' ', '_').lower())
    log_events = log_events.withColumn("page", get_page(log_events['page']))

    return log_events


# transform string column to int
def create_num_df(log_events):
    num_df = log_events.select("userId", "itemInSession", "phase", "length")\
             .groupBy("userId")\
             .agg(F.count(log_events.userId).cast('int').alias("count_user_logs"),
                  F.max(log_events.itemInSession).cast('int').alias("max_session"),
                  F.avg(log_events.length).cast('int').alias("avg_length"))
    print(num_df)
    return


