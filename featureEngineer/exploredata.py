# import libraries
from pyspark.sql import Window
import pyspark.sql.functions as F
from pyspark.sql.types import IntegerType


def down_selection(page):
    if page == 'Cancellation Confirmation' or page == 'Downgrade':
        return 1
    return 0


def create_churn_col(log_events):
    flag_downgrade_event = F.udf(down_selection, IntegerType())
    log_events = log_events.withColumn("churn", flag_downgrade_event(log_events["page"]))
    print("  create churn column  ")
    return log_events


def create_phase_col(log_events):
    # for each user order actions between certain events
    windowval = Window.partitionBy("userId").orderBy(F.desc("ts")).rangeBetween(Window.unboundedPreceding, 0)
    log_events = log_events.withColumn("phase", F.sum("churn").over(windowval))
    print('  create phase column   ')
    return log_events


def churn_analyse(log_events):
    # obtain average length one user listens to a song
    user_length = log_events.groupBy("userId").agg({"churn": "sum", "length": "mean"}).withColumnRenamed("avg(length)",
                                                                                                         "avg_length")
    # user_length.printSchema()
    # user_length.where(col("sum(churn)") == 1).describe().show()
    return user_length

