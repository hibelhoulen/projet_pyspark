import pandas as pd


def save_metrics(evaluator, yhat_test):
    metrics = {}
    metrics['f1'] = evaluator.evaluate(yhat_test, {evaluator.metricName: "f1"})
    metrics['accuracy'] = evaluator.evaluate(yhat_test, {evaluator.metricName: "accuracy"})
    metrics['weighted_precision'] = evaluator.evaluate(yhat_test, {evaluator.metricName: "weightedPrecision"})
    metrics['weighted_recall'] = evaluator.evaluate(yhat_test, {evaluator.metricName: "weightedRecall"})
    metrics_df = pd.DataFrame.from_dict(metrics, orient='index', columns=['metrics'])
    conf_matrix = yhat_test.groupby("label").pivot("prediction").count().toPandas()

    return metrics_df, conf_matrix


def save_rf_metrics(evaluator, yhat_test):
    metrics = {}
    metrics['f1'] = evaluator.evaluate(yhat_test, {evaluator.metricName: "f1"})
    metrics['accuracy'] = evaluator.evaluate(yhat_test, {evaluator.metricName: "accuracy"})
    metrics['weighted_precision'] = evaluator.evaluate(yhat_test, {evaluator.metricName: "weightedPrecision"})
    metrics['weighted_recall'] = evaluator.evaluate(yhat_test, {evaluator.metricName: "weightedRecall"})
    metrics_df = pd.DataFrame.from_dict(metrics, orient='index', columns=['metrics'])
    conf_matrix = yhat_test.groupby("label").pivot("prediction").count().toPandas()

    return metrics_df, conf_matrix

